#include <stdio.h>
#include <stdlib.h>

long fib(int num)
{
    if ((num == 1) || (num == 2))
        return 1;
    return (fib(num-1) + fib(num-2));
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Missing parameter\r\n");
        return 1;
    }
   
    printf("Fibonacci %d number is %ld\r\n", atoi(argv[1]), fib(atoi(argv[1])));
    return 0;
}

